let rec nTimesApp (f,env,n) = if (n > 0) then (f (nTimesApp (f,env,n-1)))
else env

exception No_such_variable

let initEnv v = raise No_such_variable


type exp = Var of string |
       C0 | C1 | C2 | C3 | C4 | C5 | C6 | C7 | C8 | C9 |
       Add of exp * exp | Sub of exp * exp | EQ of stmt * exp
and stmt = Assign of exp * exp | If of exp * exp * stmt * stmt
| For of exp * stmt | SQ of stmt * stmt | Noop

let rec mexp exp env = match exp with
                          (Var v) -> ((env v), env)
                         | C0 -> (0, env)
                         | C1 -> (1, env)
                         | C2 -> (2, env)
                         | C3 -> (3, env)
                         | C4 -> (4, env)
                         | C5 -> (5, env)
                         | C6 -> (6, env)
                         | C7 -> (7, env)
                         | C8 -> (8, env)
                         | C9 -> (9, env)
                         | (Add (e1, e2)) ->  let (v1, env1) = (mexp e1 env) in let (v2, env2) = (mexp e2 env1) in ((v1 + v2), env2)
                         | (Sub (e1, e2)) ->  let (v1, env1) = (mexp e1 env) in let (v2, env2) = (mexp e2 env1) in ((v1 - v2), env2)
                         | (EQ (s, e)) -> let env1 = mcmd s env in mexp e env1

and mcmd cmd env = match cmd with
                       (Assign (Var v, e)) ->  let (v1, env1) = mexp e env in (fun x -> if(x = v) then v1 else env1 x)
                     | (If (e1, e2, st1, st2)) -> if (mexp e1 env = mexp e2 env) then mcmd st1 env else mcmd st2 env
                     | (For (e, st)) -> let (v1, env1) = mexp e env in nTimesApp ((mcmd st), env1, v1)
                     | (SQ (s1, s2)) -> (let env1 = mcmd s1 env in mcmd s2 env1)
                     | Noop -> env
                     | _ -> env

let prog = If (C2,C3, Assign (Var "a", C1), Assign (Var "b", C0))
let env0 = mcmd prog initEnv

let prog1 = SQ (Assign (Var "a", C2), SQ (Assign (Var "b", Add (Var "a", C3)), Assign (Var "c", Var "b")))
let env1 = mcmd prog1 initEnv

let prog2 = Assign (Var "a", EQ (Assign (Var "b", C1), Var "b"))
let env2 = mcmd prog2 initEnv

let _ = Printf.printf "%d\n" (env2 "a")
